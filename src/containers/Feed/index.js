import React from 'react';
import { SingleDataDisplay } from './components/SingleDataDisplay';
import { MOCKED_SINGLE_DATA_COMPONENTS } from './const';
import { StyledFeed } from './styledComponents/StyledFeed';

export const Feed = () => (
    <StyledFeed>
        {MOCKED_SINGLE_DATA_COMPONENTS.map(({
            icon,
            dataName,
            value,
            timeStamp
        }) => (
            <SingleDataDisplay
                icon={icon}
                dataName={dataName}
                value={value}
                timeStamp={timeStamp}
            />
        ))}
    </StyledFeed>
);
