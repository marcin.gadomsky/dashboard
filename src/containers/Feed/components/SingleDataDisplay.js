import React from 'react';
import PropTypes from 'prop-types';

import { StyledSingleDataDisplay } from '../styledComponents/StyledSingleDataDisplay';

export const SingleDataDisplay = ({
    icon, dataName, value, timeStamp
}) => (
    <StyledSingleDataDisplay>
        <div>
            {icon}
        </div>
        <div>
            <h1>{dataName}</h1>
            <h2>{value}</h2>
            <hr />
            Last
            {' '}
            {timeStamp}
        </div>
    </StyledSingleDataDisplay>
);

SingleDataDisplay.propTypes = {
    icon: PropTypes.node,
    dataName: PropTypes.string,
    value: PropTypes.number,
    timeStamp: PropTypes.string
};

SingleDataDisplay.defaultProps = {
    icon: <div />,
    dataName: '',
    value: 0,
    timeStamp: ''
};
