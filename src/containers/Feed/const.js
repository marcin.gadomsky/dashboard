import React from 'react';

import CameraAltOutlinedIcon from '@material-ui/icons/CameraAltOutlined';
import TrackChangesOutlinedIcon from '@material-ui/icons/TrackChangesOutlined';
import HowToVoteOutlinedIcon from '@material-ui/icons/HowToVoteOutlined';
import DonutSmallOutlinedIcon from '@material-ui/icons/DonutSmallOutlined';

export const SINGLE_DATA_ICON_STYLE = {
    color: 'deepskyblue',
    fontSize: '60'
};

export const DATA_TYPES = {
    RECORDINGS: {
        name: 'RECORDINGS',
        icon: <CameraAltOutlinedIcon style={SINGLE_DATA_ICON_STYLE} />
    },
    HEATMAPS: {
        name: 'HEATMAPS',
        icon: <TrackChangesOutlinedIcon style={SINGLE_DATA_ICON_STYLE} />
    },
    FEEDBACK: {
        name: 'FEEDBACK',
        icon: <HowToVoteOutlinedIcon style={SINGLE_DATA_ICON_STYLE} />
    },
    ERRORS: {
        name: 'ERRORS',
        icon: <DonutSmallOutlinedIcon style={SINGLE_DATA_ICON_STYLE} />
    }
};

export const TIMESTAMPS = {
    DAY: '24 hours',
    WEEK: 'week',
    MONTH: 'month',
    YEAR: 'year'
};

export const MOCKED_SINGLE_DATA_COMPONENTS = [
    {
        icon: DATA_TYPES.RECORDINGS.icon,
        dataName: DATA_TYPES.RECORDINGS.name,
        value: 142,
        timeStamp: TIMESTAMPS.DAY
    },
    {
        icon: DATA_TYPES.HEATMAPS.icon,
        dataName: DATA_TYPES.HEATMAPS.name,
        value: 33,
        timeStamp: TIMESTAMPS.DAY
    },
    {
        icon: DATA_TYPES.FEEDBACK.icon,
        dataName: DATA_TYPES.FEEDBACK.name,
        value: 25,
        timeStamp: TIMESTAMPS.DAY
    },
    {
        icon: DATA_TYPES.ERRORS.icon,
        dataName: DATA_TYPES.ERRORS.name,
        value: 3,
        timeStamp: TIMESTAMPS.DAY
    }
];
