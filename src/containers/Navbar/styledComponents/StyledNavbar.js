import styled from 'styled-components';

export const StyledNavbar = styled.div`
    height: 8%;
    background-image: linear-gradient(to right, deepskyblue , violet);
    border-top-right-radius: 25px;
    border-top-left-radius: 25px;
    align-items: center;

    display: flex;
    justify-content: flex-end;
`;
