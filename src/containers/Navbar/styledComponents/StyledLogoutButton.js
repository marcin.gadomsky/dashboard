import styled from 'styled-components';

export const StyledLogoutButton = styled.button`
    border-radius: 25px;
    background-color: white;
    color: deepskyblue;
    margin: 20px;
    width: 10%;
    height: 35%;
    font-weight: bolder;
    font-size: 20px;
    border: none;
    outline: none;
`;
