import styled from 'styled-components';

export const ButtonWithIcon = styled.div`
  display: flex;
  color: white;
  margin: 20px;
`;
