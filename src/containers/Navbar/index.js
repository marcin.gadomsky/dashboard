import React from 'react';

import { StyledNavbar } from './styledComponents/StyledNavbar';
import { LogoutButton } from './components/LogoutButton';
import { NotificationsButton } from './components/NotificationsButton';
import { SettingsButton } from './components/SettingsButton';
import { HelpButton } from './components/HelpButton';

export const Navbar = () => (
    <StyledNavbar>
        <NotificationsButton />
        <HelpButton />
        <SettingsButton />
        <LogoutButton />
    </StyledNavbar>
);
