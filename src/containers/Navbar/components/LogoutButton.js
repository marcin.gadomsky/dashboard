import React from 'react';

import { StyledLogoutButton } from '../styledComponents/StyledLogoutButton';

export const LogoutButton = () => {
    const LOGOUT_BUTTON_TEXT = 'Log out';
    return (
        <StyledLogoutButton>
            {LOGOUT_BUTTON_TEXT}
        </StyledLogoutButton>
    );
};
