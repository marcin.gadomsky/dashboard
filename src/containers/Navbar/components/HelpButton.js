import React from 'react';

import PeopleOutlineIcon from '@material-ui/icons/PeopleOutline';
import { ButtonWithIcon } from '../styledComponents/ButtonWithIcon';

export const HelpButton = () => (
    <ButtonWithIcon>
        <PeopleOutlineIcon style={{ color: 'white', marginTop: '8px', fontSize: '33px' }} />
        <p>Help</p>
    </ButtonWithIcon>
);
