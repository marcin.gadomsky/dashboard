import React from 'react';

import SettingsApplicationsOutlinedIcon from '@material-ui/icons/SettingsApplicationsOutlined';
import { ButtonWithIcon } from '../styledComponents/ButtonWithIcon';

export const SettingsButton = () => (
    <ButtonWithIcon>
        <SettingsApplicationsOutlinedIcon style={{ color: 'white', marginTop: '8px', fontSize: '33px' }} />
        <p>SETTINGS</p>
    </ButtonWithIcon>
);
