import React from 'react';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';

export const NotificationsButton = () => (
    <Badge badgeContent={4} color="primary" style={{ margin: '20px', color: 'white' }}>
        <NotificationsIcon />
    </Badge>
);
