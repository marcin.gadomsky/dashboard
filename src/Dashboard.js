import React from 'react';

import { StyledDashboard } from './StyledDashboard';
import { Navbar } from './containers/Navbar';
import { LeftSidebar } from './containers/LeftSidebar';
import { Feed } from './containers/Feed';

export const Dashboard = () => (
    <StyledDashboard>
        <Navbar />
        <LeftSidebar />
        <Feed />
    </StyledDashboard>
);
