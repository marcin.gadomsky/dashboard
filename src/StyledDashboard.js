import styled from 'styled-components';

export const StyledDashboard = styled.div`
   margin: 15%;
   margin-top: 5%;
   background: white;
   height: 800px;
   border-radius: 25px;
`;
